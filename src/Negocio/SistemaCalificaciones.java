/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.Estudiante;
import Util.LeerMatriz_Excel;
import java.io.IOException;

/**
 *  Clase del negocio para la manipulación de una lista de Calificaciones
 * @author Marco Adarme
 * @author Jose Florez
 */
public class SistemaCalificaciones {
    
    private Estudiante []listaEstructuras;

    public SistemaCalificaciones() {
    }
    
    
    public SistemaCalificaciones(String nombreArchivo) throws IOException {
        LeerMatriz_Excel miExcel=new LeerMatriz_Excel(nombreArchivo,0);
        String miMatriz[][]=miExcel.getMatriz();
        
        //Normalizar: Pasar de un archivo a un modelo de objetos
        this.listaEstructuras=new Estudiante[miMatriz.length-1];
        crearEstudiantes(miMatriz);
    }
    
    
    private void crearEstudiantes(String datos[][])
    {
    
        for(int fila=1;fila<datos.length;fila++)
        {
            //Datos para un estudiante
            String nombre="";
            long codigo=0;
            float quices[]=new float[datos[fila].length-2];
            int indice_quices=0;
            for(int columna=0;columna<datos[fila].length;columna++)
            {
                if(columna==0)
                    codigo=Long.parseLong(datos[fila][columna]);
                else
                {
                    if(columna==1)
                        nombre=datos[fila][columna];
                    else
                    {
                        quices[indice_quices]=Float.parseFloat(datos[fila][columna]);
                        indice_quices++;
                    }
                }
            }
            //Creó al estudiante
            Estudiante nuevo=new Estudiante(codigo, nombre);
            nuevo.setQuices(quices);
            //Ingresar al listado de Estudiantes:
            this.listaEstructuras[fila-1]=nuevo;
        }
    }
    
    
     public SistemaCalificaciones(int can) {
         // T identi[] = new T[tamaño];
         //T es Estudiante
            this.listaEstructuras=new Estudiante[can];
    }
    
    
     //Esto va a cambiar
     public void insertarEstudiante_EnPos0(long codigo,String nombre,  String notas)
     {
         Estudiante nuevo=new Estudiante(codigo, nombre, notas);
         
         this.listaEstructuras[0]=nuevo;
         
     }

    @Override
    public String toString() {
        
        String msg="Mis estudiantes son:\n";
        
        for(Estudiante unEstudiante:this.listaEstructuras)
            msg+=unEstudiante.toString()+"\n";
        
        return msg;
    }     
     
    /**
     * Obtiene los estudiantes cuyo promedio de quices es menor a 3
     * @return un vector de objetos de la clase Estudiante
     */
    public Estudiante[] getReprobaronQuices()
    {
        return this.listaEstudiantesAproReprobados("reprobados");
    }
    
    
    /**
     * Obtiene los estudiantes cuyo promedio de quices es mayor o igual a 4
     * @return un vector de objetos de la clase Estudiante
     */
    public Estudiante[] getMayorNotaQuices()
    {
        return this.listaEstudiantesAproReprobados("aprobados");
    }
    
    /**
     * Función para contar la lista de estudiantes aprovados o reprovados
     * @param tipo: Determinar si se busca aprovación = promedio >=4 o reprovados = promedio < 3
     * @param cantEstudiantes Contador para determinar cuantos estudiantes reprueban.
     * @return Array con la lista de estudiantes aprovados o reprovados
     */
    private Estudiante[] listaEstudiantesAproReprobados(String tipo) {
        // Creo un arrary temporal con el tamaño de todos los estudiantes porque 
        // no se de cuanto puede ser el arreglo, pero si se cual es el límite
        Estudiante []estudiantesTemp = new Estudiante[this.listaEstructuras.length];
        // Contador para determinar cuantos estudiantes reprueban.
        int cantEstudiantes = 0;
        for(Estudiante est : this.listaEstructuras) {
            // Calculo promedio de estudiante
            Float promedio = est.getPromedio();
            // Si promedio menor a 3 reprobado y lo agrego al array temporal o mayor e igual a 4 aprobado
            if((tipo == "reprobados" && promedio < 3) || (tipo == "aprobados" && promedio >= 4)) {
                estudiantesTemp[cantEstudiantes] = est;
                cantEstudiantes++;
            }
        }
        // Creo el arreglo con el tamaño específico
        Estudiante []estudiantes = new Estudiante[cantEstudiantes];
        for(int i = 0; i<cantEstudiantes; i++) {
            estudiantes[i] = estudiantesTemp[i];
        }
        return estudiantes;
    }
    
    
    /**
     * Obtiene el nombre del quiz que más perdieron los estudiantes (q1, q2....qn)
     * @return un String con el nombre de la columna en Excel
     */
    public String getNombreQuiz_Perdieron()
    {
        // Suponiendo que no todos puedan tener el mismo número de quices, 
        // se itera para saber cual estudiante contiene más
        int max_quiz = 0;
        for(Estudiante est : this.listaEstructuras) {
            float []cant_quices = est.getQuices();
            if(cant_quices.length > max_quiz) {
                max_quiz = cant_quices.length;
            }
        }
        // obtenido el número de quices, se crea un array donde cada posición es un tipo de quiz, q1, q2, q3, qn
        // Ej: [12, 4, 6, 2]
        int [] quices = new int[max_quiz];
        
        // Iteramos sobre los estudiantes nuevamente
        for(Estudiante est : this.listaEstructuras) {
            float []item_quices = est.getQuices();
            for(int i = 0; i<item_quices.length; i++) {
                // Y empezamos a evaluar los quices perdidos
                if(item_quices[i]<3) {
                    // La posición i indica a cual pertenece, i=0 entonces es q1, i=1, es q2 y así sucesivamente.
                    // Y guardamos en una varibale auto-incrementandose para saber cuantas veces está de acuerdo a la posición
                    quices[i] ++;
                }
            }
        }
        
        //Como extra creamos una variable para guardar la cantidad de quices perdidos.
        int qMayor = 0;
        // el indice es la posición de estos quices.
        int indiceQMayor = 0;
        for(int i = 0; i<quices.length; i++) {
            // Si el número en la posición i es mayor que el anterior guardado entonces este pasa a ser el mayor.
            if(quices[i]>qMayor) {
                // Capturamos el indice para determinar que tipo es
                indiceQMayor = i;
                // Guardamos la cantidad como un extra para mostrar.
                qMayor = quices[i];
            }
        }
        // indice suma uno porque el array empieza en cero.
        indiceQMayor += 1;
        return "q" + indiceQMayor + " con un total de: " + qMayor;
    }
    
    
    /**
     * Obtiene la nota que más se repite  (Ojo suponga notas de un entero y un decimal
     * @return un float con la nota que más se repite
     */
    public float getNota_Que_MasRepite()
    {
        // Para notas de 0 a 5 con un solo decimal, el número máximo de posibilidades es 51 contando el 0.0
        // por ende creamos el array con todas las posibles opciones para ser guardado.
        int []notas = new int[51];
        
        // Iteramos sobre los estudiantes para obtener los indices de las notas de acuerdo a ellas.
        for(Estudiante est : this.listaEstructuras) {
            // Obtenemos los quices
            float []item_quices = est.getQuices();
            // Iteramos sobre cada uno de ellos
            for(Float q : item_quices) {
                // Convertimos a string para lo siguente:
                String sq = q+"";
                // un ejemplo de nota, 2.5 al ser un string accedemos a la primera posición 2 y la segunda posicón 5
                // Los unimos con el parseInt para que así nos formen en nuestro array la posición 25 
                // y un contador de cuantas veces estaría si llega a existir otra igual
                int indice = Integer.parseInt(sq.charAt(0) +""+ sq.charAt(2));
                notas[indice]++;
            }
        }

        // indice de la cantidad máxima de veces encontrada una nota
        int indiceNota = 0;
        // Cantidad de veces encontrada la nota
        int cantVecesNota = 0;
        // Iteramos para ver cual de todas nuestras posibles opciones almacenó la mayor cantidad de veces encontrada.
        for(int i = 0; i<notas.length; i++) {
            if(notas[i]>cantVecesNota) {
                indiceNota = i;
                cantVecesNota = notas[i];
            }
        }
        // Convertimos a string este valor para así poderle agregar el punto y convertirlo a Float.
        String no = indiceNota+"";
        if(indiceNota > 9) {
            // al ser igual a 10 este indice pasaría a ser 1.0 la nota más repetida, 
            // para el 5.0 sería la posición 50, para 1.1 la posición 11
            no = no.charAt(0) + "." + no.charAt(1);
        } else {
            // al ser menor de 10, le anteponemos el cero para idicar que es 0.9, 0.8, 0.7 hasta 0.0
            no = "0." + no.charAt(0);
        }
        Float notaR = Float.parseFloat(no);
        return notaR;
    }
}
