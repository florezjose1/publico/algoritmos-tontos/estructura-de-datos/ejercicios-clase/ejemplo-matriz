/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Modelo.Estudiante;
import Negocio.SistemaCalificaciones;
import java.io.IOException;

// Esta importación es solo para imprimir el array.
import java.util.Arrays;

/**
 * Prueba de nuevos métodos de obtención de información de las calificaciones del estudiante.
 * @author Jose Florez
 */
public class PruebaMetodosNuevos {
    public static void main(String[] args) throws IOException {
        SistemaCalificaciones sistema=new SistemaCalificaciones("src/Datos/estudiantes.xls");
        // System.out.println(sistema.toString());
        Estudiante []eR = sistema.getReprobaronQuices();
        System.out.println("Cantidad de estudiantes reprobados: " + eR.length);
        System.out.println(Arrays.toString(eR));
        
        Estudiante []eA = sistema.getMayorNotaQuices();
        System.out.println("Cantidad de estudiantes P>=4: " + eA.length);
        System.out.println(Arrays.toString(eA));
        
        
        System.out.println("Quiz más perdido: " + sistema.getNombreQuiz_Perdieron());
        
        System.out.println("Nota más repetida: " + sistema.getNota_Que_MasRepite());
        
    }
}
