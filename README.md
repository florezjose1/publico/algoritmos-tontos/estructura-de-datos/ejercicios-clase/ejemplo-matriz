# Ejemplo de matrices

El siguiente es un ejemplo básico de mostrar los alumnos y algunas funciones del sistema de calificaciones leyendo un excel.


#### Versiones:

* Apache NetBeans IDE 11.3
* Java(TM) SE Development Kit 11.0.8 



#### Funcionalidades encontradas:

* Leer el excel
* Cargar los datos del estudiante en una matriz
* Imprimir la lista de estudiantes Leer


#### Funcionalidades solicitadas `src/Vista/PruebaMetodosNuevos.java`: 

* Estudiantes con promedio menor de 3
* Estudiantes con promedio mayor o igual a 4
* Quiz mas perdido (q1, q2, q3...qn)
* Nota más repetida



Made with ♥ by [Jose Florez](www.joseflorez.co)
